# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV="${PN}-v${PV/.4/-4}"

require multibuild sourceforge [ suffix=tar.gz ]

SUMMARY="A filtering tool for a bridging firewall"
DESCRIPTION="
The ebtables program is a filtering tool for a Linux-based bridging firewall. It enables transparent
filtering of network traffic passing through a Linux bridge. The filtering possibilities are limited
to link layer filtering and some basic filtering on higher network layers. Advanced logging, MAC
DNAT/SNAT and brouter facilities are also included.

The ebtables tool can be combined with the other Linux filtering tools (iptables, ip6tables and
arptables) to make a bridging firewall that is also capable of filtering these higher network
layers. This is enabled through the bridge-netfilter architecture which is a part of the standard
Linux kernel.
"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES=""

BUGS_TO="spoonb@exherbo.org"

WORK="${WORKBASE}/${MY_PNV}"

DEFAULT_SRC_COMPILE_PARAMS=( CC="${CC}" CFLAGS="${CFLAGS}" )

src_prepare() {
    edo sed -e "/\$(DESTDIR)\$(SYSCONFIGDIR)\/ebtables-con/d"  \
            -e "/\$(DESTDIR)\$(INITDIR)\/ebtables/d"           \
            -e "s/^LIBDIR:=.*/LIBDIR:=\/usr\/$(exhost --target)\/lib\/${PN}/" \
            -e "s/^MANDIR:=.*/MANDIR:=\/usr\/share\/man/"      \
            -e "s/^BINDIR:=.*/BINDIR:=\/usr\/$(exhost --target)\/bin/" \
            -i Makefile
}

src_install() {
    default

    edo rmdir "${IMAGE}"/etc/sysconfig
    edo rmdir "${IMAGE}"/etc/{rc.d/init.d,rc.d}
}

