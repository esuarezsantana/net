# Copyright 2014-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pagure [ pn=SSSD pnv=${PN}/${PNV} ]

SUMMARY="A set of helpful libraries used by projects such as SSSD"
DESCRIPTION="
Includes:
* libbasicobjects - Basic object types for C
* libcollection - Collection data-type for C
* libdhash - Dynamic hash table
* libini_config - INI file parser for C
* libpath_utils - Filesystem Path Utilities
* libref_array - A refcounted array for C
"

LICENCES="
    GPL-3 [[ note = [ libbasicobjects ] ]]
    LGPL-3 [[ note = [ libcollection, libdhash, libini_config, libpath_utils, libref_array  ] ]]
"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~x86"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
        doc? ( app-doc/doxygen )
    test:
        dev-libs/check
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/be9ca3a2c26b061d1f22bd4a09009bba7a01f67b.patch
    "${FILES}"/a731d8c8c515e7e42a4fb448e0ecb6934d5bf99b.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
)

src_compile() {
    default

    option doc && edo emake docs
}

src_install() {
    default

    if option doc; then
        for i in basicobjects collection ini path_utils refarray; do
            insinto /usr/share/doc/${PNVR}/html/${i}
            doins -r ${i}/doc/html/*
        done
    fi
}

