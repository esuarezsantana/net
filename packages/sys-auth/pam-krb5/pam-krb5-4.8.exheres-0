# Copyright 2013 Dirk Heinrichs
# Distributed under the terms of the GNU General Public License v2

SUMMARY="PAM module providing Kerberos V authentication"
DESCRIPTION="
pam-krb5 provides a Kerberos v5 PAM module that supports authentication, user
ticket cache handling, simple authorization (via .k5login or checking Kerberos
principals against local usernames), and password changing.
"

HOMEPAGE="https://www.eyrie.org/~eagle/software/${PN}/"
DOWNLOADS="https://archives.eyrie.org/software/kerberos/${PNV}.tar.xz"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: heimdal krb5 ) [[ number-selected = exactly-one ]]
"

# The tests trigger DNS calls to search for Kerberos servers.
RESTRICT="test"

DEPENDENCIES="
    build+run:
        sys-fs/e2fsprogs
        sys-libs/pam
        providers:heimdal? ( app-crypt/heimdal )
        providers:krb5? ( app-crypt/krb5 )
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --libdir=/usr/$(exhost --target)/lib )

