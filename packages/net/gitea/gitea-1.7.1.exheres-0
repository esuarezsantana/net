# Copyright 2017-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=go-gitea tag=v${PV} ] \
    systemd-service [ systemd_files=[ contrib/systemd/gitea.service ] ]

SUMMARY="Gitea - Git with a cup of tea"
HOMEPAGE+=" https://gitea.io"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"

# require additional go packages
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/go[>=1.11]
    build+run:
        user/gitea
        group/gitea
        sys-libs/pam
    run:
        dev-scm/git[>=1.7.1]
    suggestion:
        dev-db/sqlite:3 [[
            description = [ Required for using a plain SQLite database instead of MySQL or PostgreSQL ]
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.7.1-fix-version-tag.patch
    "${FILES}"/${PN}-1.7.1-adjust-config.patch
    "${FILES}"/${PN}-1.5.0-adjust-service-file.patch
    "${FILES}"/${PN}-1.5.0-disable-u2f.patch
)

src_prepare() {
    default

    export GOROOT="/usr/$(exhost --target)/lib/go"
    export GOPATH="${WORKBASE}"/build

    edo mkdir -p "${WORKBASE}"/build/src/code.gitea.io
    edo ln -s "${WORK}" "${WORKBASE}"/build/src/code.gitea.io/gitea
}

src_compile() {
    edo pushd "${WORKBASE}"/build/src/code.gitea.io/gitea

    edo go fix
    edo go build \
        -o bin/gitea \
        -tags='production sqlite pam cert'

    edo popd
}

src_install() {
    dobin "${WORKBASE}"/build/src/code.gitea.io/gitea/bin/gitea

    insinto /usr/share/gitea
    doins -r {public,templates}
    insinto /usr/share/gitea/options
    doins -r options/{gitignore,label,license,readme}

    insinto /etc/gitea
    newins custom/conf/app.ini.sample app.ini
    edo chown -R gitea:gitea "${IMAGE}"/etc/gitea

    keepdir /var/{lib,log}/gitea
    keepdir /var/lib/gitea/custom

    insinto /var/lib/gitea/conf
    doins -r options/locale

    edo chown gitea:gitea "${IMAGE}"/var/{lib,log}/gitea

    install_systemd_files
}

