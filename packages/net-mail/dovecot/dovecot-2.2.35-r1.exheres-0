# Copyright 2008 Fernando J. Pereda
# Copyright 2009, 2010 Daniel Mierswa <impulze@impulze.org>
# Copyright 2012-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service

case "$(ever range 4)" in
    rc*|beta*|alpha*) MYPV="$(ever range 1-3).$(ever range 4)" ;;
    *) MYPV="${PV}" ;;
esac

case "$(ever range 4)" in
    rc*) DOWNLOAD_PATH="rc" ;;
    beta*) DOWNLOAD_PATH="beta" ;;
    alpha*) DOWNLOAD_PATH="alpha" ;;
esac

SUMMARY="Fast, secure, IMAP/POP server"
DESCRIPTION="
Dovecot is an open source IMAP and POP3 email server for Linux/UNIX-like systems,
written with security primarily in mind. Dovecot is an excellent choice for both
small and large installations. It's fast, simple to set up, requires no special
administration and it uses very little memory.
"
HOMEPAGE="https://www.${PN}.org"
DOWNLOADS="${HOMEPAGE}/releases/$(ever range 1-2)/${DOWNLOAD_PATH}/${PN}-${MYPV}.tar.gz"
BUGS_TO="philantrop@exherbo.org"
LICENCES="LGPL-2.1 MIT BSD-3 public-domain"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    caps
    ldap
    lz4 [[ description = [ Build with LZ4 compression support ] ]]
    mysql
    postgresql
    solr
    sqlite
    tcpd
    (
        mysql
        postgresql
        sqlite
    ) [[ description = [ Database backends for dovecot and plugins ] ]]
    solr [[ description = [ Apache Solr fast text searching backend ] ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

WORK=${WORKBASE}/${PN}-${MYPV}

DEPENDENCIES="
    build:
        app-text/unicode-data
        virtual/pkg-config
    build+run:
        sys-libs/pam
        caps? ( sys-libs/libcap )
        ldap? ( net-directory/openldap )
        lz4? ( app-arch/lz4 )
        mysql? ( virtual/mysql )
        postgresql? ( dev-db/postgresql-client )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        solr? (
            dev-libs/expat
            net-misc/curl
        )
        sqlite? ( dev-db/sqlite:3 )
        tcpd? ( sys-apps/tcp-wrappers )
        user/dovecot
        user/dovenull
    run:
        group/dovecot
        user/dovecot
    suggestion:
        net-mail/dovecot-pigeonhole
        [[ description =
            [ Sieve plugin for deliver and Managesieve implementation. ]
        ]]
        postgresql? ( dev-db/postgresql )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    VALGRIND=no
    --localstatedir=/var
    --with-bzlib
    --with-docs
    --with-lzma
    --with-pam
    --with-rundir=/run/dovecot
    --with-shadow
    --with-ssl
    --with-systemdsystemunitdir="${SYSTEMDSYSTEMUNITDIR}"
    --with-zlib
    --without-cassandra
    # broken
    --without-gssapi
    --without-icu
    --without-lucene
    --without-nss
    --without-stemmer
    --without-textcat
    # avoid automagic dependencies
    --without-apparmor
    --without-vpopmail
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    "caps libcap"
    ldap
    lz4
    mysql
    "postgresql pgsql"
    solr
    sqlite
    "tcpd libwrap"
)

src_unpack() {
    default

    edo cp /usr/share/unicode-data/UnicodeData.txt "${WORK}"/src/lib
}

src_test() {
    esandbox allow_net "unix:${WORK}/src/lib-program-client/program-client-test.sock"
    esandbox allow_net --connect "LOOPBACK@0"
    default
    esandbox disallow_net --connect "LOOPBACK@0"
    esandbox disallow_net "unix:${WORK}/src/lib-program-client/program-client-test.sock"
}

src_install() {
    default

    # remove pointless README which just points to /usr/share/doc/*
    edo rm "${IMAGE}"/etc/dovecot/README
    edo rmdir "${IMAGE}"/etc/dovecot

    # /etc should be empty, because init.d script won't get installed
    edo rmdir "${IMAGE}"/etc

    keepdir /usr/$(exhost --target)/lib/dovecot/auth
}

